package com.xiaoyu.tio.redis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;

import org.redisson.Redisson;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.tio.client.ClientChannelContext;
import org.tio.client.ClientGroupContext;
import org.tio.client.ReconnConf;
import org.tio.client.TioClient;
import org.tio.client.intf.ClientAioListener;
import org.tio.core.Node;
import org.tio.core.Tio;

import com.xiaoyu.tio.redis.config.TedisConfig;
import com.xiaoyu.tio.redis.core.Tedis;
import com.xiaoyu.tio.redis.core.client.TedisStandaloneClient;
import com.xiaoyu.tio.redis.core.handler.RedisHandler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		// Node serverNode = new Node("127.0.0.1", 6379);
		// RedisHandler tioClientHandler = new RedisHandler();
		// ClientAioListener aioListener = null;
		// ReconnConf reconnConf = new ReconnConf();
		// ClientGroupContext clientGroupContext = new
		// ClientGroupContext(tioClientHandler, aioListener, reconnConf);
		// TioClient aioClient = new TioClient(clientGroupContext);
		// ClientChannelContext clientChannelContext = aioClient.connect(serverNode);
		// for (int i = 0; i < 106756; i++) {
		// final int a = i;
		// Thread thread = new Thread(() -> {
		// RedisPacket packet = new RedisPacket();
		// List<String> parms = new ArrayList<>();
		// if (a % 2 == 0) {
		// packet = new RedisPacket("SET");
		// parms.add("haha");
		// parms.add("0");
		// // parms.add("2");
		// packet.setParms(parms);
		// } else {
		// packet = new RedisPacket("GET");
		// parms.add("haha");
		// // parms.add("101");
		// // parms.add("1");
		// packet.setParms(parms);
		// }
		// CompletableFuture<MessageResponse> future = new CompletableFuture<>();
		// Tio.send(clientChannelContext, packet);
		// Long id = packet.getId();
		// tioClientHandler.stackAdd(id);
		// tioClientHandler.addMessage(id, future);
		// MessageResponse response = null;
		// try {
		// response = future.get(1, TimeUnit.SECONDS);
		// if (response.getId() == id) {
		// System.out.println("线程" + a + "接收到数据：" + response.getData());
		// } else {
		// System.out.println("线程" + a + "数据接收错误！！");
		// }
		//
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// } catch (ExecutionException e) {
		// e.printStackTrace();
		// } catch (TimeoutException e) {
		// System.err.println("线程：" + a + "接收数据超时！！");
		// }
		// // while (count < 10) {
		// // data = tioClientHandler.getResData(id);
		// // if (data == null) {
		// // try {
		// // Thread.sleep(100);
		// // } catch (InterruptedException e) {
		// // // TODO Auto-generated catch block
		// // e.printStackTrace();
		// // }
		// // count++;
		// // continue;
		// // }
		// // flag = true;
		// // tioClientHandler.getData().remove(id);
		// // break;
		// // }
		// });
		// thread.start();
		// }

//		 Config c = new Config();
//		 c.useSingleServer().setAddress("redis://127.0.0.1:6379").setPassword("foobared");
//		 RedissonClient redissonClient = Redisson.create(c);
//		 RAtomicLong longObject = redissonClient.getAtomicLong("myLong");
		// TimeInterval t1 = DateUtil.timer();
		// for (int i = 0; i < 10000; i++) {
		//// final int a = i;
		//// Thread thread = new Thread(() -> {
		// long v = longObject.incrementAndGet();
		//// System.out.println("线程" + a + "接收到数据：" + v);
		//// });
		//// thread.start();
		// }
		// System.out.println("总共耗时" + t1.intervalMs() + "ms");

		TedisConfig config = new TedisConfig();
		config.host("127.0.0.1").port(6379).database(0).password("foobared");
		Tedis<String, String> client = new Tedis<String, String>(config);
		client.connect();
		TimeInterval t = DateUtil.timer();
		TedisStandaloneClient<String, String> e = client.getTedisClient();
//		e.set("ha", "12");
//		System.out.println(e.getAndSet("ha", "123"));	
		for (int i = 0; i < 10000; i++) {
			System.out.println(e.increment("test12", 1L));
		}
		client.close();
//		final AtomicLong l = new AtomicLong(0);
//		for (int i = 0; i < 100; i++) {
//			final int a = i;
//			Thread thread = new Thread(() -> {
//				try {
//					for (int j = 0; j < 1; j++) {
//						// MessageResponse messageResponse = client.sendCommand("LRANGE",
//						// "testlist".getBytes(), "0".getBytes(), "3".getBytes());
//						// System.out.println("线程" + l.incrementAndGet() + "接收到数据：" +
//						// messageResponse.getData());
//						Tedis c = client.getTedisPool().get();
//						Boolean f = c.exists("haha");
//						System.out.println(f);
//						client.getTedisPool().release(c);
//					}
//
//				} catch (TimeoutException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			});
//			thread.start();
//		}

		// System.out.println("总共耗时" + t.intervalMs() + "ms");

	}
}
