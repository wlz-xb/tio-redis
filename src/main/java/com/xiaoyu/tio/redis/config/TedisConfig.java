package com.xiaoyu.tio.redis.config;

public class TedisConfig {
	
	private String host = "127.0.0.1";
	
	private Integer port = 6379;
	
	private String password;
	
    private int database = 0;
    
	public TedisConfig database(int database) {
		this.setDatabase(database);
		return this;
	}
	
	public TedisConfig port(Integer port) {
		this.port = port;
		return this;
	}
	
	public TedisConfig password(String password) {
		this.password = password;
		return this;
	}
	
	public TedisConfig host(String host) {
		this.host = host;
		return this;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public int getDatabase() {
		return database;
	}

	public void setDatabase(int database) {
		this.database = database;
	}

}
