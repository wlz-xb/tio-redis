package com.xiaoyu.tio.redis.core;

import java.util.concurrent.TimeUnit;

/**
 * 通用命令
 * 
 * @author 小雨哥哥
 *
 * @param <K>
 * @param <V>
 */
public interface CurrencyCommand<K, V> {

	void set(K key, V value);

	void set(K key, V value, long timeout, TimeUnit unit);

	Boolean setIfAbsent(K key, V value);

	V get(K key);

	V getAndSet(K key, V value);

	Long increment(K key, long delta);

	Double increment(K key, double delta);

	Integer append(K key, String value);

	String get(K key, long start, long end);

	void set(K key, V value, long offset);

	Boolean setBit(K key, long offset, boolean value);

	Boolean getBit(K key, long offset);

	enum SetOption {
		EX, PX, NX, XX
	}

}
