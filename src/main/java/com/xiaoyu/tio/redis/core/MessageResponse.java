package com.xiaoyu.tio.redis.core;

import java.util.List;

public class MessageResponse {
	
	private Long id;
	
	private List<byte[]> data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<byte[]> getData() {
		return data;
	}

	public void setData(List<byte[]> data) {
		this.data = data;
	}
	
	

}
