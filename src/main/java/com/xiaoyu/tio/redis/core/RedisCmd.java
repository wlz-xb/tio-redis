package com.xiaoyu.tio.redis.core;

public enum RedisCmd {

	SET, SETBIT, MSET, MSETNX, PSETEX, SETNX,
	GET, HGET, MGET, GETBIT, GETRANGE, GETSET, 
	INCR, INCRBY, INCRBYFLOAT, 
	DECR, DECRBY, 
	STRLEN, 
	EXISTS, 
	APPEND, 
	BITCOUNT, BITOP, BITPOS, 
	AUTH, 
	ECHO, 
	PING, 
	QUIT, 
	SELECT,
	BLPOP,LPUSH,
	DEL;

}
