package com.xiaoyu.tio.redis.core;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.tio.core.intf.Packet;

public class RedisPacket extends Packet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 命令
	 */
	private String cmd;

	/**
	 * 参数
	 */
	private List<byte[]> parms;

	private byte[] res;
	
	private CompletableFuture<MessageResponse> future = new CompletableFuture<>();

	public RedisPacket() {
		
	}

	public RedisPacket(String cmd) {
		this.cmd = cmd;
	}

	public RedisPacket(String cmd, List<byte[]> parms) {
		this.cmd = cmd;
		this.parms = parms;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public byte[] getRes() {
		return res;
	}

	public void setRes(byte[] res) {
		this.res = res;
	}

	public List<byte[]> getParms() {
		return parms;
	}

	public void setParms(List<byte[]> parms) {
		this.parms = parms;
	}

	public CompletableFuture<MessageResponse> getFuture() {
		return future;
	}

}
