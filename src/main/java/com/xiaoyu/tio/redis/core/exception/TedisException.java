package com.xiaoyu.tio.redis.core.exception;

public class TedisException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    public TedisException(String msg) {
        super(msg);
    }


    public TedisException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TedisException(Throwable cause) {
        super(cause);
    }

}
