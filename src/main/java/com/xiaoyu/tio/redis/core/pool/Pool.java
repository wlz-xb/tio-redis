package com.xiaoyu.tio.redis.core.pool;

public interface Pool<T> {

	T get();
	
	void release(T c);
	
	
	
}
