package com.xiaoyu.tio.redis.core.protocol;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class StringCodec implements TedisCodec<String, String, String>{

	private final Charset charset;
	
	public StringCodec() {
		this(StandardCharsets.UTF_8);
	}
	
	public StringCodec(Charset charset) {
		this.charset = charset;
	}
	
	@Override
	public byte[] encoderKey(String k) {
		return k.getBytes(charset);
	}

	@Override
	public byte[] encoderValue(String v) {
		return v.getBytes(charset);
	}

	@Override
	public String decoderKey(byte[] k) {
		return  k == null ? null : new String(k, charset);
	}

	@Override
	public String decoderValue(byte[] v) {
		return v == null ? null : new String(v, charset);
	}


	@Override
	public byte[] encoderHash(String h) {
		return h.getBytes(charset);
	}

	@Override
	public String decoderHash(byte[] h) {
		return h == null ? null : new String(h, charset);
	}

}
