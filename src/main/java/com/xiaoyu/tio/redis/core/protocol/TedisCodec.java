package com.xiaoyu.tio.redis.core.protocol;

public interface TedisCodec<H, K, V> {

	byte[] encoderHash(H h);
	
	byte[] encoderKey(K k);
	
	byte[] encoderValue(V v);
	
	
	H decoderHash(byte[] h);
	
	K decoderKey(byte[] k);
	
	V decoderValue(byte[] v);
	
	
}
